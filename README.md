# DFL Config Parser

This library provides simple to use parsers for various config files.
Currently, a color scheme parser implementation is available which can parse KDE and Qt5Ct color schemes.


### Dependencies:
* <tt>Qt Core (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>
* [DFL::ColorUtils](https://gitlab.com/desktop-frameworks/color-utils)
* [Wayfire](https://github.com/WayfireWM/wayfire.git)
* [WfConfig](https://github.com/WayfireWM/wf-config.git)

**Note:** Suitable versions of Wayfire and WfConfig are required to compile WayfireParser class.
1. If you do not have wayfire/wf-config installed, then this class will not be compiled/installed.
2. If you do not want WayfireParser class compiled despite having Wayfire/WfConfig installed, pass
  the `-Dbuild_wfparser=false` to `meson`.

### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/config-parser dfl-config-parser`
- Enter the `dfl-config-parser` folder
  * `cd dfl-config-parser`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release -Duse_qt_version=qt5`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know


### Upcoming
* Create Dark and Light Color Schemes for a given set of base colors.
* Any other feature you request for... :)
