/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * The DFL::Config::Wayfire class parses wayfire configuration file (ini and hjson)
 * and returns the values stored in it as a QVariantMap.
 **/

#include <dirent.h>
#include <unistd.h>
#include <sys/inotify.h>

#include <filesystem>
#include <cstring>
#include <any>
#include <set>

#include <QColor>

#include "hjson.h"
#include "HjsonUtils.hpp"
#include "DFHjsonParser.hpp"

#include "DFWayfireParser.hpp"
#include "XmlReader.hpp"
#include "WfConfigUtils.hpp"

#include <wayfire/config.h>

DFL::Config::Wayfire::Wayfire( QString usrConf, DFL::Config::Wayfire::Format format ) {
    mError = NoError;

    WayfireXmlParser reader;

    /** Store it in @mSystemConfig */
    mSystemConfig = reader.allOptions();

    /** Hjson format */
    if ( format == DFL::Config::Wayfire::HjsonFormat ) {
        /** Read the distro defaults */
        if ( QFile::exists( SYSCONFDIR "/xdg/wayfire/defaults.hjson" ) ) {
            mUserConfig = DFL::Config::Hjson::readConfigFromFile( SYSCONFDIR "/xdg/wayfire/defaults.hjson" );
        }

        /** Override everything with user settings */
        mUserConfig = DFL::Config::Hjson::readConfigFromFile( usrConf );
    }

    /** Ini format */
    else {
        if ( QFile::exists( SYSCONFDIR "/xdg/wayfire/defaults.ini" ) ) {
            mUserConfig = loadConfigFromIni( SYSCONFDIR "/xdg/wayfire/defaults.ini" );
        }

        /** Override everything with user settings */
        mUserConfig = loadConfigFromIni( usrConf );
    }
}


QVariantMap DFL::Config::Wayfire::config( DFL::Config::Wayfire::ConfigType type ) {
    switch ( type ) {
        case DFL::Config::Wayfire::WayfireDefaults: {
            return mSystemConfig;
        }

        case DFL::Config::Wayfire::UserSettings: {
            return mUserConfig;
        }
    }

    return QVariantMap();
}


bool DFL::Config::Wayfire::saveAs( QString usrConf, DFL::Config::Wayfire::Format format ) {
    switch ( format ) {
        /** Use DFL::Config::HjsonParser to do the writing */
        case DFL::Config::Wayfire::HjsonFormat: {
            return DFL::Config::Hjson::writeConfigToFile( mUserConfig, usrConf );
        }

        /** We will write the ini file manually */
        case DFL::Config::Wayfire::IniFormat: {
            QFile usrCfg( usrConf );

            if ( usrCfg.open( QFile::WriteOnly ) != true ) {
                qWarning() << "Unable to open file for writing" << usrConf;
                return false;
            }

            usrCfg.write( configAsIni( mUserConfig ).toUtf8() );

            usrCfg.close();

            return true;
        }
    }

    return false;
}


bool DFL::Config::Wayfire::setValue( QString sectName, QString optName, std::any value ) {
    qDebug() << std::any( (char *)"test" ).type().name();
    qDebug() << std::any( std::string( "test" ) ).type().name();

    /** No such section */
    if ( mUserConfig.keys().contains( sectName ) == false ) {
        return false;
    }

    /** No such option */
    QVariantMap section = mUserConfig[ sectName ].toMap();

    if ( section.keys().contains( optName ) == false ) {
        return false;
    }

    QVariantMap option = section[ optName ].toMap();

    QString typeName = value.type().name();

    /** Save color */
    if ( typeName.endsWith( "QColor" ) ) {
        //
    }

    /** Save */
    else if ( typeName.endsWith( "" ) ) {
        //
    }

    else if ( typeName.endsWith( "" ) ) {
        //
    }

    else {
        //
    }

    return false;
}


QVariantMap DFL::Config::Wayfire::readConfigFromFile( QString filename ) {
    DFL::Config::Wayfire parser( filename, (filename.endsWith( ".hjson" ) ? HjsonFormat : IniFormat) );

    return parser.config( UserSettings );
}


bool DFL::Config::Wayfire::writeConfigToFile( QVariantMap settingsMap, QString filename ) {
    Q_UNUSED( settingsMap );
    Q_UNUSED( filename );

    return true;
}


QString DFL::Config::Wayfire::writeConfigToString( QVariantMap settingsMap ) {
    ::Hjson::Value cfgMap;

    for ( QString key: settingsMap.keys() ) {
        cfgMap[ key.toStdString() ] = convertQVariant( settingsMap[ key ] );
    }

    ::Hjson::EncoderOptions encOpts;

    encOpts.indentBy      = "\t";
    encOpts.unknownAsNull = true;

    std::string hjsonStr;

    try {
        hjsonStr = ::Hjson::Marshal( cfgMap, encOpts );
    } catch ( ::Hjson::file_error& err ) {
        qDebug() << "Error writing configuration file.";
        qDebug() << err.what();
        return "";
    }

    return QString( hjsonStr.c_str() );
}


QVariantMap DFL::Config::Wayfire::loadConfigFromIni( QString usrCfg ) {
    /** If we have not loaded the system config no use continuing */
    if ( mSystemConfig.keys().count() == 0 ) {
        qDebug() << "Please load the Wayfire defaults first.";
        return QVariantMap();
    }

    /** Copy the system config, and then override the differing values */
    QVariantMap newCfg( mSystemConfig );

    QSettings cfg( usrCfg, QSettings::IniFormat );

    /** Get all the sections */
    QStringList sections = cfg.childGroups();

    /** Get all the sections */
    QStringList defSects = newCfg.keys();

    /**
     * For each of the sections, we list all the config keys.
     * Load the values from keys which are not of dynamic-list type.
     * The remaining values are of dynamic-list types.
     */
    for ( QString sectName: sections ) {
        /** Most likely a per-output option. */
        if ( sectName.contains( ":" ) ) {
            QString base = sectName.split( ":" ).at( 0 );

            if ( defSects.contains( base ) != true ) {
                continue;
            }
        }

        /** Unknown option, or an error */
        else if ( defSects.contains( sectName ) != true ) {
            continue;
        }

        QVariantMap section;

        if ( sectName.contains( ":" ) ) {
            section = newCfg[ sectName.split( ":" ).at( 0 ) ].toMap();
        }

        else {
            section = newCfg[ sectName ].toMap();
        }

        // /** per-output section get special treatment */
        // bool isSpecial = sectName.contains( ":" );

        cfg.beginGroup( sectName );

        QStringList unreadKeys = cfg.allKeys();

        /** These are the "normal" options */
        for ( QString optName: section.keys() ) {
            QVariantMap option = section[ optName ].toMap();

            if ( option[ "type" ].toString() != "dynamic-list" ) {
                unreadKeys.removeAll( optName );

                if ( cfg.contains( optName ) ) {
                    option[ "value" ]  = cfg.value( optName );
                    section[ optName ] = option;
                }
            }
        }

        /**
         * Stage 1: single/multi dicts
         * We parse the dynamic lists of dict type.
         */
        for ( QString optName: section.keys() ) {
            QVariantMap option = section[ optName ].toMap();

            /** Do not process further if this option is not a dynamic-list */
            if ( option[ "type" ].toString() != "dynamic-list" ) {
                continue;
            }

            /** Process single-dict: has only one entry => suffix will be the name */
            if ( option[ "type-hint" ].toString() == "single-dict" ) {
                QVariantMap entry  = option[ "entries" ].toList().at( 0 ).toMap();
                QString     prefix = entry[ "prefix" ].toString();

                QVariantMap values;
                for ( QString key: QStringList( unreadKeys ) ) {
                    if ( key.startsWith( prefix ) ) {
                        QString name = QString( key ).remove( 0, prefix.length() );
                        values[ name ] = cfg.value( key );
                    }
                }

                option[ "value" ]  = values;
                section[ optName ] = option;
            }

            /**
             * Process multi-dict
             * We need to do a double-pass.
             * First pass: collect all the suffixes which match atleast one of the prefixes.
             * Second pass: collect all keys of a given suffix and check if keys can be paired with the
             * prefixes.
             * Only the keys that remain after the second pass will be parsed.
             */
            else if ( option[ "type-hint" ].toString() == "multi-dict" ) {
                /** Store the prefixes and the corresponding names */
                QStringList prefixes, names;

                /** Get the entries */
                QVariantList entries = option[ "entries" ].toList();

                /** Reserve space for storing the values */
                QVariantList values;

                /** Get the prefixes */
                for ( QVariant entryVar: entries ) {
                    QVariantMap entry = entryVar.toMap();
                    prefixes << entry[ "prefix" ].toString();
                    names << entry[ "name" ].toString();
                }

                /** First pass: Get all the suffixes */
                QStringList suffixes;
                for ( QString prefix: prefixes ) {
                    for ( QString key: unreadKeys ) {
                        if ( key.startsWith( prefix ) ) {
                            QString suffix = QString( key ).remove( 0, prefix.length() );

                            if ( suffixes.contains( suffix ) != true ) {
                                suffixes << suffix;
                            }
                        }
                    }
                }

                /** Second pass */
                for ( QString suffix: QStringList( suffixes ) ) {
                    QStringList goodChoices, goodKeys;
                    for ( QString key: unreadKeys ) {
                        QString goodChoice( key );

                        if ( goodChoice.endsWith( suffix ) == true ) {
                            goodChoice.chop( suffix.length() );
                            goodChoices << goodChoice;
                            goodKeys << key;
                        }
                    }

                    bool goodMatch = true;
                    for ( QString goodChoice: goodChoices ) {
                        goodMatch &= prefixes.contains( goodChoice );
                    }

                    if ( goodMatch == true ) {
                        QVariantMap newOpt;
                        for ( QString key: goodKeys ) {
                            QString prefix = QString( key ).replace( suffix, "" );
                            QString name   = names.value( prefixes.indexOf( prefix ) );
                            newOpt[ name ] = cfg.value( key );

                            unreadKeys.removeAll( key );
                        }
                        newOpt[ "<suffix>" ] = suffix;

                        values << newOpt;
                    }
                }

                option[ "value" ]  = values;
                section[ optName ] = option;
            }
        }

        /**
         * Stage 2: Plain lists
         * We parse the dynamic lists for type plain.
         * We make two assumption currently:
         *   1. Any given section has only one plain-list option
         *   2. All unaccounted keys of ini belong to this option
         */
        QStringList plainTypes;
        for ( QString optName: section.keys() ) {
            QVariantMap option = section[ optName ].toMap();

            /**
             * Process this option only if it's a plain list
             * It will have only one entry => single prefix
             */
            if ( (option[ "type" ].toString() == "dynamic-list") and (option[ "type-hint" ].toString() == "plain") ) {
                /** Count the plain-list type options */
                plainTypes << optName;

                /** Store the prefixes and the corresponding names */
                QVariantList entries = option[ "entries" ].toList();

                if ( entries.count() != 1 ) {
                    qWarning() << "Please ensure the xml is correctly formatted.";
                    qWarning() << optName << "can have only 1 entry, found" << entries.count();
                }

                QVariantList newOpt;
                for ( QString key: unreadKeys ) {
                    newOpt << cfg.value( key );
                }

                option[ "value" ]  = newOpt;
                section[ optName ] = option;
            }
        }

        /** Inform the user if more than one plain list type was found */
        if ( plainTypes.length() > 1 ) {
            qWarning() << sectName << "contained more than 1 'plain' dynamic-list option:";
            qWarning() << plainTypes.join( " " ).toUtf8().constData();
            qWarning() << "Please inform the devs of DFL::ConfigParser about this development.";
        }

        newCfg[ sectName ] = section;
        cfg.endGroup();
    }

    return newCfg;
}
