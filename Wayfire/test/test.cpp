/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * A very simple app for testing.
 **/

#include <any>
#include <QtGui>

#include "XmlReader.hpp"
#include "WfConfigUtils.hpp"
#include "DFWayfireParser.hpp"

int main( int, char ** ) {
    DFL::Config::Wayfire wfParser( "/home/cosmos/.config/wayfire.ini", DFL::Config::Wayfire::IniFormat );

    /** We want the user settings */
    QVariantMap wfCfg = wfParser.config( DFL::Config::Wayfire::UserSettings );

    qDebug() << wfCfg.keys();

    /** [animate] */
    QVariantMap animateCfg = wfCfg[ "animate" ].toMap();

    for ( QString key: animateCfg.keys() ) {
        qDebug() << key << ":" << animateCfg[ key ].toMap()[ "value" ].toString();
    }

    /** [output:HDMI-A-1] */
    QVariantMap opHdmiCfg = wfCfg[ "output:HDMI-A-1" ].toMap();

    for ( QString key: opHdmiCfg.keys() ) {
        qDebug() << key << ":" << opHdmiCfg[ key ].toMap()[ "value" ].toString();
    }

    /**
     * Here we use wfCfg and show the settings GUI to the user.
     * The user makes changes to the settings, and we store it back in wfCfg.
     */

    /** Wew now save the user configuration into an ini file */
    wfParser.saveAs( "/tmp/wayfire.ini", DFL::Config::Wayfire::IniFormat );

    wfParser.setValue( "test", "test", std::any( QColor( 100, 100, 100, 100 ) ) );

    // WayfireXmlParser reader;
    //
    // qDebug() << reader.allSections();
    // qDebug() << reader.optionsInSection( "autostart" );
    //
    // QVariantMap allOpts = reader.allOptions();
    //
    // QVariantMap autostart = allOpts[ "command" ].toMap();
    // QVariantMap asOpt     = autostart[ "bindings" ].toMap();
    //
    // qDebug() << asOpt.keys();
    //
    // for ( QVariant entry: asOpt[ "entries" ].toList() ) {
    //     QVariantMap entryMap = entry.toMap();
    //     qDebug() << "Name   :" << entryMap[ "name" ].toString();
    //     qDebug() << "Type   :" << entryMap[ "type" ].toString();
    //     qDebug() << "Prefix :" << entryMap[ "prefix" ].toString();
    //     qDebug() << "Hint   :" << entryMap[ "hint" ].toString();
    //     qDebug() << "";
    // }
    //
    // QFile ini( QDir::home().filePath( "wayfire.ini" ) );
    //
    // if ( ini.open( QFile::WriteOnly ) ) {
    //     ini.write( configAsIni( allOpts ).toUtf8() );
    //     ini.close();
    // }

    return 0;
}
