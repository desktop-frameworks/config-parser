/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * The DFL::Config::Wayfire class parses wayfire configuration file (ini and hjson)
 * and returns the values stored in it as a QVariantMap.
 **/

#pragma once

#include <QtCore>

namespace DFL {
    namespace Config {
        class Wayfire;
    }
}

class DFL::Config::Wayfire {
    public:
        enum Error {
            NoError,                    // There is no error
            SyntaxError,                // The syntax of the config file was incorrect.
            FileError,                  // The config file was not found.
            UnknownError                // We do not know what the error is.
        };

        /** We need only two types: Wayfire default and user */
        enum ConfigType {
            WayfireDefaults,            // Defaults shipped by Wayfire
            UserSettings,               // User defined configuration
        };

        /** Currently, we support two formats */
        enum Format {
            IniFormat,                  // The default Wayfire format
            HjsonFormat,                // Hjson format
        };

        /**
         * @usrConf - File where the user configuration is stored
         * @format  - Format of the configuration file
         */
        Wayfire( QString usrConf, DFL::Config::Wayfire::Format format );

        /**
         * Get the configuration as QVariantMap.
         */
        QVariantMap config( DFL::Config::Wayfire::ConfigType type );

        /**
         * Store a value as string
         */
        bool setValue( QString section, QString option, std::any value );

        /**
         * Save the configuration loaded in the configuration to a @usrConf.
         */
        bool saveAs( QString usrConf, DFL::Config::Wayfire::Format format );

        /**
         * Read the configuration stored in the file @filename,
         * and store them in QVariantMap object.
         * Note: Previously loaded configurations will not be modified, or used.
         * This function is provided for use with DFL::Settings.
         */
        static QVariantMap readConfigFromFile( QString filename );

        /**
         * Write the configuration stored QVariantMap object into a file @filename.
         * This function is provided for use with DFL::Settings.
         */
        static bool writeConfigToFile( QVariantMap settingMap, QString filename );

        /**
         * Write the configuration stored QVariantMap object into a file @filename.
         * This function is provided for use with DFL::Settings.
         */
        static QString writeConfigToString( QVariantMap settingMap );

    private:
        /** App defaults: Do not change after the first read */
        QVariantMap mSystemConfig;

        /** User config: Rewrite this at each reload */
        QVariantMap mUserConfig;

        /** Stores the last reported error. */
        int mError = -1;

        /** Parse an INI file and load the options into a QVariantMap */
        QVariantMap loadConfigFromIni( QString );
};
