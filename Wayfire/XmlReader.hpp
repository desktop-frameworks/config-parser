/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * WayfireXmlParser class parses the Wayfire metadata XML files and generates
 * a default configuration QVariantMap.
 **/

#pragma once

#include <QtCore>

class WayfireXmlParser {
    public:
        WayfireXmlParser();

        /** List of all sections */
        QStringList allSections();

        /** All options of a section */
        QStringList optionsInSection( QString );

        /** All the options */
        QVariantMap allOptions();

    private:
        QXmlStreamReader xml;

        QVariantMap mOptions;

        /**
         * Read an option and generate an option map
         * Each option map will have the following keys:
         *  - type (int/string/bool/color...)
         *  - type-hint (to be used with type=dynamic-list)
         *  - short (short description)
         *  - long (long description)
         *  - entries (A map, see below for keys)
         *  - value (default value if it exists)
         *  - range (applicable only for int/double)
         *  - choices
         *
         * Each compound option entry will be a map having the keys:
         *  - prefix
         *  - type
         *  - hint (optional)
         *  - name (optional)
         */
        QVariantMap readOption();

        /**
         * Read the allowed values (choices)
         * We need two entries
         *  1. <value>
         *  2. <_name> (optional)
         */
        QVariantMap readOptionChoice();

        /** Extract the entry */
        QVariantMap readCompoundOptionEntry();
};
