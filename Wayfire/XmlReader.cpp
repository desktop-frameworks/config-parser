/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * WayfireXmlParser class parses the Wayfire metadata XML files and generates
 * a default configuration QVariantMap.
 **/

#include <wayfire/config.h>

#include "XmlReader.hpp"

static QStringList getXmlDirs() {
    QStringList xmlDirs;

    /** User defined */
    xmlDirs = qEnvironmentVariable( "WAYFIRE_PLUGIN_XML_PATH" ).split( ":" );

    /** Locally installed */
    if ( QDir( qEnvironmentVariable( "XDG_DATA_HOME" ) ).exists() ) {
        xmlDirs << qEnvironmentVariable( "XDG_DATA_HOME" ) + "/wayfire/metadata";
    }

    else if ( QDir( qEnvironmentVariable( "HOME" ) + "/.local/share/" ).exists() ) {
        xmlDirs << qEnvironmentVariable( "HOME" ) + "/.local/share/wayfire/metadata";
    }

    /** Default values */
    xmlDirs << PLUGIN_XML_DIR;

    return xmlDirs;
}


WayfireXmlParser::WayfireXmlParser() {
    for ( QString xmlPath: getXmlDirs() ) {
        QDir xmlDir( xmlPath );
        for ( QString xmlFile: xmlDir.entryList( { "*.xml" }, QDir::Files ) ) {
            QFile f( xmlDir.filePath( xmlFile ) );

            if ( f.open( QFile::ReadOnly ) != true ) {
                qWarning() << "Unable to read the xml file:" << xmlFile.toUtf8().constData();
                continue;
            }

            xml.setDevice( &f );

            /** Plugin name */
            QString plugin;

            /** <wayfire> */
            if ( xml.readNextStartElement() != true ) {
                qWarning() << "Invalid XML file:" << xmlFile.toUtf8().constData();
            }

            else if ( xml.name() != QStringLiteral( "wayfire" ) ) {
                qWarning() << "Not a wayfire plugin XML file:" << xmlFile.toUtf8().constData();
            }

            /** <plugin> or <object> */
            if ( xml.readNextStartElement() != true ) {
                qWarning() << "Invalid XML file:" << xmlFile.toUtf8().constData();
            }

            else if ( (xml.name() != QStringLiteral( "plugin" ) ) and (xml.name() != QStringLiteral( "object" ) ) ) {
                qWarning() << "No plugin/object information found:" << xmlFile.toUtf8().constData();
                qWarning() << "Expected <plugin ...> or <object ...> element. Found" << xml.name();
            }

            else {
                plugin = xml.attributes().value( "name" ).toString();
                QVariantMap init;
                init[ "section-type" ] = xml.name().toString();
                mOptions[ plugin ]     = init;
            }

            bool    addOptionToGroup = false;
            QString group;

            /** Read till the end of the document */
            while ( xml.atEnd() != true ) {
                /** Read the next element */
                xml.readNextStartElement();

                /** Get the previously stored map */
                QVariantMap optionMap = mOptions[ plugin ].toMap();

                /** Plugin short description */
                if ( xml.name() == QStringLiteral( "_short" ) ) {
                    optionMap[ "short" ] = xml.readElementText();
                }

                /** Plugin long description */
                else if ( xml.name() == QStringLiteral( "_long" ) ) {
                    optionMap[ "long" ] = xml.readElementText();
                }

                /** Option group */
                else if ( xml.name() == QStringLiteral( "category" ) ) {
                    optionMap[ "category" ] = xml.readElementText();
                }

                /** Option group */
                else if ( xml.name() == QStringLiteral( "group" ) ) {
                    /** Start of a group */
                    if ( xml.tokenType() == QXmlStreamReader::StartElement ) {
                        xml.readNextStartElement();

                        if ( xml.name() == QStringLiteral( "_short" ) ) {
                            addOptionToGroup = true;
                            group            = xml.readElementText();
                        }
                    }

                    /** End of a group */
                    else if ( xml.tokenType() == QXmlStreamReader::EndElement ) {
                        addOptionToGroup = false;
                        group            = QString();
                    }

                    /** Go to next tag */
                    continue;
                }

                /** If it is not <option ...>, skip it */
                else if ( xml.name() == QStringLiteral( "option" ) ) {
                    if ( xml.tokenType() == QXmlStreamReader::EndElement ) {
                        continue;
                    }

                    QString option = xml.attributes().value( "name" ).toString();

                    /** If this option belongs to a group, let's add it to a group */
                    if ( addOptionToGroup and group.length() ) {
                        QVariantMap  groups     = optionMap[ "groups" ].toMap();
                        QVariantList groupItems = groups[ group ].toList();
                        groupItems << option;
                        groups[ group ]       = groupItems;
                        optionMap[ "groups" ] = groups;
                    }

                    optionMap[ option ] = readOption();
                }

                mOptions[ plugin ] = optionMap;
            }
        }
    }
}


QStringList WayfireXmlParser::allSections() {
    return mOptions.keys();
}


QStringList WayfireXmlParser::optionsInSection( QString sect ) {
    return mOptions[ sect ].toMap().keys();
}


QVariantMap WayfireXmlParser::allOptions() {
    return mOptions;
}


QVariantMap WayfireXmlParser::readOption() {
    QVariantMap option;

    /** Get the attributes */
    option[ "type" ]      = xml.attributes().value( "type" ).toString();
    option[ "type-hint" ] = xml.attributes().value( "type-hint" ).toString();

    /** Enter the options */
    xml.readNextStartElement();

    /** Progress to the next element, until we are at the end of the option */
    while ( true ) {
        /** Process this tag further only when it's a new start element */
        if ( xml.tokenType() == QXmlStreamReader::StartElement ) {
            if ( xml.name() == QStringLiteral( "_short" ) ) {
                option[ "short" ] = xml.readElementText();
            }

            else if ( xml.name() == QStringLiteral( "_long" ) ) {
                option[ "long" ] = xml.readElementText();
            }

            else if ( xml.name() == QStringLiteral( "default" ) ) {
                option[ "value" ] = xml.readElementText();
            }

            else if ( xml.name() == QStringLiteral( "min" ) ) {
                option[ "min" ] = xml.readElementText();
            }

            else if ( xml.name() == QStringLiteral( "max" ) ) {
                option[ "max" ] = xml.readElementText();
            }

            else if ( xml.name() == QStringLiteral( "desc" ) ) {
                QVariantList choices = option[ "choices" ].toList();
                choices << readOptionChoice();
                option[ "choices" ] = choices;
            }

            else if ( xml.name() == QStringLiteral( "entry" ) ) {
                QVariantList entries = option[ "entries" ].toList();
                entries << readCompoundOptionEntry();
                option[ "entries" ] = entries;
            }
        }

        /** Read the next tag */
        xml.readNextStartElement();

        /** When the tag is end element tag, and it's the end of the option, stop looping */
        if ( xml.tokenType() == QXmlStreamReader::EndElement and xml.name() == QStringLiteral( "option" ) ) {
            break;
        }
    }

    /** Post processing: Determine if we have a single dict, or multi-dict */
    if ( option[ "type-hint" ].toString() == "dict" ) {
        if ( option[ "entries" ].toList().length() == 1 ) {
            option[ "type-hint" ] = "single-dict";
        }

        else if ( option[ "entries" ].toList().length() > 1 ) {
            option[ "type-hint" ] = "multi-dict";
        }

        /** Something wrong with the XML? */
        else {
            option[ "type-hint" ] = "";
        }
    }

    return option;
}


QVariantMap WayfireXmlParser::readOptionChoice() {
    /** We will still be at <desc> */
    xml.readNextStartElement();
    QVariantMap choice;

    while ( true ) {
        /** Process this tag further only when it's a new start element */
        if ( xml.tokenType() == QXmlStreamReader::StartElement ) {
            if ( xml.name() == QStringLiteral( "value" ) ) {
                choice[ "value" ] = xml.readElementText();
            }

            else if ( xml.name() == QStringLiteral( "_name" ) ) {
                choice[ "descr" ] = xml.readElementText();
            }
        }

        /** Read the next tag */
        xml.readNextStartElement();

        /** When the tag is end element tag, and it's the end of the desc, stop looping */
        if ( xml.tokenType() == QXmlStreamReader::EndElement and xml.name() == QStringLiteral( "desc" ) ) {
            break;
        }
    }

    return choice;
}


QVariantMap WayfireXmlParser::readCompoundOptionEntry() {
    /** Get prefix, type, name from <entry ...> */
    QVariantMap entry;

    entry[ "prefix" ] = xml.attributes().value( "prefix" ).toString();
    entry[ "type" ]   = xml.attributes().value( "type" ).toString();
    entry[ "name" ]   = xml.attributes().value( "name" ).toString();

    /** Read the next tag */
    xml.readNextStartElement();

    /**
     * This check is required due to inconsistent format of wayfire's XML files.
     * Entry can either be formatted as <entry .... /> or <entry> .... </entry>.
     * This checks for the former.
     */
    if ( xml.tokenType() == QXmlStreamReader::EndElement and xml.name() == QStringLiteral( "entry" ) ) {
        entry[ "hint" ] = "";
        return entry;
    }

    while ( true ) {
        /** Process this tag further only when it's a new start element */
        if ( (xml.tokenType() == QXmlStreamReader::StartElement) and (xml.name() == QStringLiteral( "hint" ) ) ) {
            entry[ "hint" ] = xml.readElementText();
        }

        /** Read the next tag */
        xml.readNextStartElement();

        /** When the tag is end element tag, and it's the end of the entry, stop looping */
        if ( xml.tokenType() == QXmlStreamReader::EndElement and xml.name() == QStringLiteral( "entry" ) ) {
            break;
        }
    }

    return entry;
}
