/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * Some useful function which are used in DFL::Config::Wayfire
 **/

#include <QtCore>

#include "WfConfigUtils.hpp"

/**
 * For compound options which are of type plain,
 * we will not have any prefix. We will use a default
 * generated option name : opt_#N
 */
const QString listPrefix( "opt_" );

const QStringList badOptNames = { "short", "long", "category", "groups", "section-type" };

QString configAsIni( QVariantMap config ) {
    QString ini;

    QStringList sectionNames = config.keys();

    sectionNames.sort();

    for ( QString sectName: sectionNames ) {
        /** Write the section name */
        ini += QString( "[%1]\n" ).arg( sectName );

        /** Get the options */
        QVariantMap options = config[ sectName ].toMap();

        /** Get the alphabetically sorted option names */
        QStringList optionNames = options.keys();
        optionNames.sort();

        /** Start storing each option into ini */
        for ( QString optName: optionNames ) {
            if ( badOptNames.contains( optName ) ) {
                continue;
            }

            QVariantMap option = options[ optName ].toMap();

            /** Compound options */
            if ( option[ "type" ] == "dynamic-list" ) {
                if ( option[ "type-hint" ] == "plain" ) {
                    int width = QString::number( option[ "value" ].toList().length() ).length();

                    uint counter = 1;
                    for ( QString entry: option[ "value" ].toStringList() ) {
                        ini     += QString( "%1%2 = %3\n" ).arg( listPrefix ).arg( counter, width, 10, QLatin1Char( '0' ) ).arg( entry );
                        counter += 1;
                    }
                }

                /**
                 * Single entry dicts are of type QVariantMap
                 */
                else if ( option[ "type-hint" ].toString() == "single-dict" ) {
                    /** Get the prefixes */
                    QVariantMap entry  = option[ "entries" ].toList().at( 0 ).toMap();
                    QString     prefix = entry[ "prefix" ].toString();

                    QVariantMap values = option[ "value" ].toMap();
                    for ( QString key: values.keys() ) {
                        ini += QString( "%1%2 = %3\n" ).arg( prefix ).arg( key ).arg( values[ key ].toString() );
                    }
                }

                /**
                 * For INI, single/multi element dict are the same.
                 * dynamic-list of dict type will be stored as a list of dicts
                 */
                else if ( option[ "type-hint" ].toString() == "multi-dict" ) {
                    QStringList prefixes, names;
                    for ( QVariant entryVar: option[ "entries" ].toList() ) {
                        QVariantMap entry = entryVar.toMap();
                        prefixes << entry[ "prefix" ].toString();
                        names << entry[ "name" ].toString();
                    }

                    for ( QVariant valVar: option[ "value" ].toList() ) {
                        /** Get the values */
                        QVariantMap value = valVar.toMap();

                        /** Get our suffix */
                        QString suffix = value.take( "<suffix>" ).toString();

                        /** Write the entries into the ini string. */
                        for ( QString key: value.keys() ) {
                            for ( QString name: names ) {
                                if ( key == name ) {
                                    QString prefix = prefixes.at( names.indexOf( name ) );
                                    ini += QString( "%1%2 = %3\n" ).arg( prefix ).arg( suffix ).arg( value[ key ].toString() );
                                }
                            }
                        }
                    }
                }

                else {
                    qWarning() << "Unknown type of dynamic-list";
                    qWarning() << sectName << optName << option[ "value" ];
                }
            }

            /** Simple options */
            else {
                ini += QString( "%1 = %2\n" ).arg( optName ).arg( option[ "value" ].toString() );
            }
        }

        ini += "\n";
    }

    /**
     * We do not have comments at the moment. So # means #, not comment.
     * Replace all instances of '#' with '\#'.
     */
    // ini = ini.replace( "#", "#" );

    return ini;
}
