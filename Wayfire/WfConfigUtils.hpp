/**
 * Copyright (c) 2023
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * Some useful function which are used in DFL::Config::Wayfire
 **/

#include <QList>
#include <QString>
#include <QVariant>

QString configAsIni( QVariantMap );
