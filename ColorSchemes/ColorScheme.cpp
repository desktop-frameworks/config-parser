/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Config::ColorScheme class parses KDE and Qt5CT color schemes
 * and returns a QPalette for it.
 **/

#include <QtCore>
#include <QtGui>

#include <DFColorUtils.hpp>
#include "DFColorScheme.hpp"

/** Fixed search paths. */
QStringList DFL::Config::ColorScheme::mSchemePaths = {
    QDir::home().filePath( ".local/share/color-schemes" ),
    QDir::home().filePath( ".config/qt5ct/colors" ),
    "/usr/local/share/color-schemes",
    "/usr/share/color-schemes",
    "/usr/share/qt5ct/colors"
};

/** Internal ENUMS for easy handling of values */
enum IntensityEffects {
    IntensityNoEffect,
    IntensityShade,
    IntensityDarken,
    IntensityLighten,
    NIntensityEffects,
};

enum ColorEffects {
    ColorNoEffect,
    ColorDesaturate,
    ColorFade,
    ColorTint,
    NColorEffects,
};

enum ContrastEffects {
    ContrastNoEffect,
    ContrastFade,
    ContrastTint,
    NContrastEffects,
};

/**
 * Convert (r,g,b) or (r,g,b,a) color tuple into QColor
 */
QColor asColor( QSettings *scheme, QString key ) {
    QStringList rgba = scheme->value( key ).toStringList();

    if ( rgba.size() != 3 and rgba.size() != 4 ) {
        return QColor( Qt::gray );
    }

    bool ok = false;
    int  r  = rgba.at( 0 ).toInt( &ok, 10 );

    if ( not ok ) {
        return QColor( Qt::gray );
    }

    int g = rgba.at( 1 ).toInt( &ok, 10 );

    if ( not ok ) {
        return QColor( Qt::gray );
    }

    int b = rgba.at( 2 ).toInt( &ok, 10 );

    if ( not ok ) {
        return QColor( Qt::gray );
    }

    int a = 255;

    if ( rgba.size() == 4 ) {
        a = rgba.at( 3 ).toInt( &ok, 10 );

        if ( not ok ) {
            return QColor( Qt::gray );
        }
    }

    return QColor( r, g, b, a );
}


/**
 * Convert Colors:Window/BackgroundNormal into various shades.
 */
QColor getShade( QSettings *scheme, QPalette::ColorRole role ) {
    QColor bgColor( asColor( scheme, "Colors:Window/BackgroundNormal" ) );
    qreal  contrast = 0.1 * scheme->value( "KDE/contrast", 7 ).toInt();

    contrast = (1.0 > contrast ? (-1.0 < contrast ? contrast : -1.0) : 1.0);
    qreal y  = DFL::ColorUtils::luma( bgColor );
    qreal yi = 1.0 - y;

    // handle very dark colors (base, mid, dark, shadow == Midlight, light)
    if ( y < 0.006 ) {
        switch ( role ) {
            case QPalette::Light: {
                return DFL::ColorUtils::shade( bgColor, 0.05 + 0.95 * contrast );
            }

            case QPalette::Mid: {
                return DFL::ColorUtils::shade( bgColor, 0.01 + 0.20 * contrast );
            }

            case QPalette::Dark: {
                return DFL::ColorUtils::shade( bgColor, 0.02 + 0.40 * contrast );
            }

            default: {
                return DFL::ColorUtils::shade( bgColor, 0.03 + 0.60 * contrast );
            }
        }
    }

    // handle very light colors (base, Midlight, light == mid, dark, shadow)
    if ( y > 0.93 ) {
        switch ( role ) {
            case QPalette::Midlight: {
                return DFL::ColorUtils::shade( bgColor, -0.02 - 0.20 * contrast );
            }

            case QPalette::Dark: {
                return DFL::ColorUtils::shade( bgColor, -0.06 - 0.60 * contrast );
            }

            case QPalette::Shadow: {
                return DFL::ColorUtils::shade( bgColor, -0.10 - 0.90 * contrast );
            }

            default: {
                return DFL::ColorUtils::shade( bgColor, -0.04 - 0.40 * contrast );
            }
        }
    }

    // handle everything else
    qreal lightAmount = (0.05 + y * 0.55) * (0.25 + contrast * 0.75);
    qreal darkAmount  = (-y) * (0.55 + contrast * 0.35);

    switch ( role ) {
        case QPalette::Light: {
            return DFL::ColorUtils::shade( bgColor, lightAmount );
        }

        case QPalette::Midlight: {
            return DFL::ColorUtils::shade( bgColor, (0.15 + 0.35 * yi) * lightAmount );
        }

        case QPalette::Mid: {
            return DFL::ColorUtils::shade( bgColor, (0.35 + 0.15 * y) * darkAmount );
        }

        case QPalette::Dark: {
            return DFL::ColorUtils::shade( bgColor, darkAmount );
        }

        default: {
            return DFL::ColorUtils::darken( DFL::ColorUtils::shade( bgColor, darkAmount ), 0.5 + 0.3 * y );
        }
    }

    /** Just in case... Also to suppress compiler warnings */
    return bgColor;
}


/**
 * Generate Inactive/Disabled colors from Active Colors
 */
QColor generateColor( QSettings *scheme, QPalette::ColorGroup state, QPalette::ColorRole role, bool foreground = true ) {
    QColor bgColor( asColor( scheme, "Colors:Window/BackgroundNormal" ) );
    QColor fgColor;

    /** The other color, and it's effects with which we will mix/tint/shade */
    QColor altColor;
    qreal  colorAmount;
    int    colorEffect;
    qreal  contrastAmount;
    int    contrastEffect;
    qreal  intensityAmount;
    int    intensityEffect;

    /** Inactive palette */
    if ( state == QPalette::Inactive ) {
        altColor        = asColor( scheme, "ColorEffects:Inactive/Color" );
        colorAmount     = scheme->value( "ColorEffects:Inactive/ColorAmount", 0.025 ).toReal();
        colorEffect     = scheme->value( "ColorEffects:Inactive/ColorEffect", 2 ).toInt();
        contrastAmount  = scheme->value( "ColorEffects:Inactive/ContrastAmount", 0.1 ).toReal();
        contrastEffect  = scheme->value( "ColorEffects:Inactive/ContrastEffect", 2 ).toInt();
        intensityAmount = scheme->value( "ColorEffects:Inactive/IntensityAmount", 0.0 ).toReal();
        intensityEffect = scheme->value( "ColorEffects:Inactive/IntensityEffect", 0 ).toInt();
    }

    /** Disabled palette */
    else {
        altColor        = asColor( scheme, "ColorEffects:Disabled/Color" );
        colorAmount     = scheme->value( "ColorEffects:Disabled/ColorAmount", 0 ).toReal();
        colorEffect     = scheme->value( "ColorEffects:Disabled/ColorEffect", 2 ).toInt();
        contrastAmount  = scheme->value( "ColorEffects:Disabled/ContrastAmount", 0.65 ).toReal();
        contrastEffect  = scheme->value( "ColorEffects:Disabled/ContrastEffect", 2 ).toInt();
        intensityAmount = scheme->value( "ColorEffects:Disabled/IntensityAmount", 0.1 ).toReal();
        intensityEffect = scheme->value( "ColorEffects:Disabled/IntensityEffect", 0 ).toInt();
    }

    QColor color;

    /** Generate Foreground Colors */
    if ( foreground ) {
        switch ( role ) {
            /** Only the contrast of the Foreground is changed */
            case QPalette::WindowText: {
                fgColor = asColor( scheme, "Colors:Window/ForegroundNormal" );
                break;
            }

            case QPalette::Text: {
                fgColor = asColor( scheme, "Colors:View/ForegroundNormal" );
                break;
            }

            case QPalette::ButtonText: {
                fgColor = asColor( scheme, "Colors:Button/ForegroundNormal" );
                break;
            }

            case QPalette::HighlightedText: {
                fgColor = asColor( scheme, "Colors:Selection/ForegroundNormal" );
                break;
            }

            case QPalette::ToolTipText: {
                fgColor = asColor( scheme, "Colors:Tooltip/ForegroundNormal" );
                break;
            }

            case QPalette::Link: {
                fgColor = asColor( scheme, "Colors:View/ForegroundLink" );
                break;
            }

            case QPalette::LinkVisited: {
                fgColor = asColor( scheme, "Colors:View/ForegroundVisited" );
                break;
            }

            case QPalette::PlaceholderText: {
                fgColor = asColor( scheme, "Colors:View/ForegroundInactive" );
                break;
            }

            /** We need to generate this properly */
            case QPalette::BrightText: {
                fgColor = asColor( scheme, "Colors:View/ForegroundNormal" );
                break;
            }

            /** We should never be coming here */
            default: {
                return QColor( Qt::gray );
            }
        }

        color = fgColor;
        switch ( contrastEffect ) {
            case ContrastFade: {
                color = DFL::ColorUtils::mix( fgColor, bgColor, contrastAmount );
                break;
            }

            case ContrastTint: {
                color = DFL::ColorUtils::tint( fgColor, bgColor, contrastAmount );
                break;
            }
        }
    }

    /** Generate Background Colors */
    else {
        switch ( role ) {
            /** Background Roles suffer both Intensity and Color effects */
            case QPalette::Light: {
                bgColor = getShade( scheme, QPalette::Light );
                break;
            }

            case QPalette::Midlight: {
                bgColor = getShade( scheme, QPalette::Midlight );
                break;
            }

            case QPalette::Mid: {
                bgColor = getShade( scheme, QPalette::Mid );
                break;
            }

            case QPalette::Dark: {
                bgColor = getShade( scheme, QPalette::Dark );
                break;
            }

            case QPalette::Shadow: {
                bgColor = getShade( scheme, QPalette::Shadow );
                break;
            }

            case QPalette::Window: {
                bgColor = asColor( scheme, "Colors:Window/BackgroundNormal" );
                break;
            }

            case QPalette::Base: {
                bgColor = asColor( scheme, "Colors:View/BackgroundNormal" );
                break;
            }

            case QPalette::Button: {
                bgColor = asColor( scheme, "Colors:Button/BackgroundNormal" );
                break;
            }

            case QPalette::AlternateBase: {
                bgColor = asColor( scheme, "Colors:View/BackgroundAlternate" );
                break;
            }

            case QPalette::ToolTipBase: {
                bgColor = asColor( scheme, "Colors:Tooltip/BackgroundNormal" );
                break;
            }

            case QPalette::NoRole: {
                bgColor = asColor( scheme, "Colors:Window/BackgroundNormal" );
                break;
            }

            case QPalette::Highlight: {
                bgColor = asColor( scheme, "Colors:Selection/BackgroundNormal" );
                break;
            }

            /** We should never be coming here */
            default: {
                return QColor( Qt::gray );
            }
        }

        color = bgColor;
        switch ( intensityEffect ) {
            case IntensityShade: {
                color = DFL::ColorUtils::shade( bgColor, intensityAmount );
                break;
            }

            case IntensityDarken: {
                color = DFL::ColorUtils::darken( bgColor, intensityAmount );
                break;
            }

            case IntensityLighten: {
                color = DFL::ColorUtils::lighten( bgColor, intensityAmount );
                break;
            }

            default: {
                break;
            }
        }

        switch ( colorEffect ) {
            case ColorDesaturate: {
                color = DFL::ColorUtils::darken( color, 0.0, 1.0 - colorAmount );
                break;
            }

            case ColorFade: {
                color = DFL::ColorUtils::mix( color, altColor, colorAmount );
                break;
            }

            case ColorTint: {
                color = DFL::ColorUtils::tint( color, altColor, colorAmount );
                break;
            }

            default: {
                break;
            }
        }
    }

    return color;
}


/**
 * ColorScheme parser
 */
DFL::Config::ColorScheme::ColorScheme( QString cs ) {
    mColorScheme = cs;
    QString path = getSchemePath();

    if ( not path.isEmpty() ) {
        mPalette = new QPalette();

        if ( path.endsWith( ".colors" ) ) {
            parseKdeColorScheme( path );
        }

        else {
            parseQt5CtColorScheme( path );
        }
    }
}


/**
 * Return the generate palette
 */
QPalette DFL::Config::ColorScheme::palette() {
    if ( mPalette ) {
        return *mPalette;
    }

    return QPalette();
}


void DFL::Config::ColorScheme::addColorSchemePaths( QStringList paths ) {
    for ( QString path: paths ) {
        if ( QFile::exists( path ) ) {
            while ( path.endsWith( "/" ) ) {
                path.chop( 1 );
            }

            mSchemePaths << path;
        }
    }

    mSchemePaths.removeDuplicates();
}


QStringList DFL::Config::ColorScheme::colorSchemes() {
    QStringList schemes;

    for ( QString path: mSchemePaths ) {
        QDirIterator it( path, { "*.conf", "*.colors" }, QDir::Files );
        while ( it.hasNext() ) {
            it.next();
            schemes << it.fileInfo().baseName();
        }
    }

    schemes.removeDuplicates();

    return schemes;
}


QString DFL::Config::ColorScheme::getSchemePath() {
    /** We have a full-path here */
    if ( QFile::exists( mColorScheme ) ) {
        return mColorScheme;
    }

    for ( QString path: mSchemePaths ) {
        QString kdeClrPath = path + "/" + mColorScheme + ".colors";

        if ( QFile::exists( kdeClrPath ) ) {
            return kdeClrPath;
        }

        QString qt5ctClrPath = path + "/" + mColorScheme + ".conf";

        if ( QFile::exists( qt5ctClrPath ) ) {
            return qt5ctClrPath;
        }
    }

    /** Color Scheme not found */
    return QString();
}


void DFL::Config::ColorScheme::parseKdeColorScheme( QString file ) {
    QSettings *scheme = new QSettings( file, QSettings::IniFormat );

    /** Palette for Active Widgets */
    mPalette->setColor( QPalette::Active,   QPalette::WindowText,      asColor( scheme, "Colors:Window/ForegroundNormal" ) );
    mPalette->setColor( QPalette::Active,   QPalette::Window,          asColor( scheme, "Colors:Window/BackgroundNormal" ) );
    mPalette->setColor( QPalette::Active,   QPalette::Base,            asColor( scheme, "Colors:View/BackgroundNormal" ) );
    mPalette->setColor( QPalette::Active,   QPalette::Text,            asColor( scheme, "Colors:View/ForegroundNormal" ) );
    mPalette->setColor( QPalette::Active,   QPalette::Button,          asColor( scheme, "Colors:Button/BackgroundNormal" ) );
    mPalette->setColor( QPalette::Active,   QPalette::ButtonText,      asColor( scheme, "Colors:Button/ForegroundNormal" ) );
    mPalette->setColor( QPalette::Active,   QPalette::Highlight,       asColor( scheme, "Colors:Selection/BackgroundNormal" ) );
    mPalette->setColor( QPalette::Active,   QPalette::HighlightedText, asColor( scheme, "Colors:Selection/ForegroundNormal" ) );
    mPalette->setColor( QPalette::Active,   QPalette::ToolTipBase,     asColor( scheme, "Colors:Tooltip/BackgroundNormal" ) );
    mPalette->setColor( QPalette::Active,   QPalette::ToolTipText,     asColor( scheme, "Colors:Tooltip/ForegroundNormal" ) );
    mPalette->setColor( QPalette::Active,   QPalette::Light,           getShade( scheme, QPalette::Light ) );
    mPalette->setColor( QPalette::Active,   QPalette::Midlight,        getShade( scheme, QPalette::Midlight ) );
    mPalette->setColor( QPalette::Active,   QPalette::Mid,             getShade( scheme, QPalette::Mid ) );
    mPalette->setColor( QPalette::Active,   QPalette::Dark,            getShade( scheme, QPalette::Dark ) );
    mPalette->setColor( QPalette::Active,   QPalette::Shadow,          getShade( scheme, QPalette::Shadow ) );
    mPalette->setColor( QPalette::Active,   QPalette::AlternateBase,   asColor( scheme, "Colors:View/BackgroundAlternate" ) );
    mPalette->setColor( QPalette::Active,   QPalette::Link,            asColor( scheme, "Colors:View/ForegroundLink" ) );
    mPalette->setColor( QPalette::Active,   QPalette::LinkVisited,     asColor( scheme, "Colors:View/ForegroundVisited" ) );
    mPalette->setColor( QPalette::Active,   QPalette::PlaceholderText, asColor( scheme, "Colors:View/ForegroundInactive" ) );
    mPalette->setColor( QPalette::Active,   QPalette::NoRole,          asColor( scheme, "Colors:Window/BackgroundNormal" ) );
    mPalette->setColor( QPalette::Active,   QPalette::BrightText,      asColor( scheme, "Colors:Window/ForegroundNormal" ) );

    /** Palette for Inactive Widgets */
    mPalette->setColor( QPalette::Inactive, QPalette::WindowText,      generateColor( scheme, QPalette::Inactive, QPalette::WindowText, true ) );
    mPalette->setColor( QPalette::Inactive, QPalette::Window,          generateColor( scheme, QPalette::Inactive, QPalette::Window, false ) );
    mPalette->setColor( QPalette::Inactive, QPalette::Base,            generateColor( scheme, QPalette::Inactive, QPalette::Base, false ) );
    mPalette->setColor( QPalette::Inactive, QPalette::Text,            generateColor( scheme, QPalette::Inactive, QPalette::Text, true ) );
    mPalette->setColor( QPalette::Inactive, QPalette::Button,          generateColor( scheme, QPalette::Inactive, QPalette::Button, false ) );
    mPalette->setColor( QPalette::Inactive, QPalette::ButtonText,      generateColor( scheme, QPalette::Inactive, QPalette::ButtonText, true ) );
    mPalette->setColor( QPalette::Inactive, QPalette::Highlight,       generateColor( scheme, QPalette::Inactive, QPalette::Highlight, false ) );
    mPalette->setColor( QPalette::Inactive, QPalette::HighlightedText, generateColor( scheme, QPalette::Inactive, QPalette::HighlightedText, true ) );
    mPalette->setColor( QPalette::Inactive, QPalette::ToolTipBase,     generateColor( scheme, QPalette::Inactive, QPalette::ToolTipBase, false ) );
    mPalette->setColor( QPalette::Inactive, QPalette::ToolTipText,     generateColor( scheme, QPalette::Inactive, QPalette::ToolTipText, true ) );
    mPalette->setColor( QPalette::Inactive, QPalette::Light,           generateColor( scheme, QPalette::Inactive, QPalette::Light, false ) );
    mPalette->setColor( QPalette::Inactive, QPalette::Midlight,        generateColor( scheme, QPalette::Inactive, QPalette::Midlight, false ) );
    mPalette->setColor( QPalette::Inactive, QPalette::Mid,             generateColor( scheme, QPalette::Inactive, QPalette::Mid, false ) );
    mPalette->setColor( QPalette::Inactive, QPalette::Dark,            generateColor( scheme, QPalette::Inactive, QPalette::Dark, false ) );
    mPalette->setColor( QPalette::Inactive, QPalette::Shadow,          generateColor( scheme, QPalette::Inactive, QPalette::Shadow, false ) );
    mPalette->setColor( QPalette::Inactive, QPalette::AlternateBase,   generateColor( scheme, QPalette::Inactive, QPalette::AlternateBase, false ) );
    mPalette->setColor( QPalette::Inactive, QPalette::Link,            generateColor( scheme, QPalette::Inactive, QPalette::Link, true ) );
    mPalette->setColor( QPalette::Inactive, QPalette::LinkVisited,     generateColor( scheme, QPalette::Inactive, QPalette::LinkVisited, true ) );
    mPalette->setColor( QPalette::Inactive, QPalette::PlaceholderText, generateColor( scheme, QPalette::Inactive, QPalette::PlaceholderText, true ) );
    mPalette->setColor( QPalette::Inactive, QPalette::NoRole,          generateColor( scheme, QPalette::Inactive, QPalette::NoRole, false ) );
    mPalette->setColor( QPalette::Inactive, QPalette::BrightText,      generateColor( scheme, QPalette::Inactive, QPalette::BrightText, false ) );

    /** Palette for Disabled Widgets */
    mPalette->setColor( QPalette::Disabled, QPalette::WindowText,      generateColor( scheme, QPalette::Disabled, QPalette::WindowText, true ) );
    mPalette->setColor( QPalette::Disabled, QPalette::Window,          generateColor( scheme, QPalette::Disabled, QPalette::Window, false ) );
    mPalette->setColor( QPalette::Disabled, QPalette::Base,            generateColor( scheme, QPalette::Disabled, QPalette::Base, false ) );
    mPalette->setColor( QPalette::Disabled, QPalette::Text,            generateColor( scheme, QPalette::Disabled, QPalette::Text, true ) );
    mPalette->setColor( QPalette::Disabled, QPalette::Button,          generateColor( scheme, QPalette::Disabled, QPalette::Button, false ) );
    mPalette->setColor( QPalette::Disabled, QPalette::ButtonText,      generateColor( scheme, QPalette::Disabled, QPalette::ButtonText, true ) );
    mPalette->setColor( QPalette::Disabled, QPalette::Highlight,       generateColor( scheme, QPalette::Disabled, QPalette::Highlight, false ) );
    mPalette->setColor( QPalette::Disabled, QPalette::HighlightedText, generateColor( scheme, QPalette::Disabled, QPalette::HighlightedText, true ) );
    mPalette->setColor( QPalette::Disabled, QPalette::ToolTipBase,     generateColor( scheme, QPalette::Disabled, QPalette::ToolTipBase, false ) );
    mPalette->setColor( QPalette::Disabled, QPalette::ToolTipText,     generateColor( scheme, QPalette::Disabled, QPalette::ToolTipText, true ) );
    mPalette->setColor( QPalette::Disabled, QPalette::Light,           generateColor( scheme, QPalette::Disabled, QPalette::Light, false ) );
    mPalette->setColor( QPalette::Disabled, QPalette::Midlight,        generateColor( scheme, QPalette::Disabled, QPalette::Midlight, false ) );
    mPalette->setColor( QPalette::Disabled, QPalette::Mid,             generateColor( scheme, QPalette::Disabled, QPalette::Mid, false ) );
    mPalette->setColor( QPalette::Disabled, QPalette::Dark,            generateColor( scheme, QPalette::Disabled, QPalette::Dark, false ) );
    mPalette->setColor( QPalette::Disabled, QPalette::Shadow,          generateColor( scheme, QPalette::Disabled, QPalette::Shadow, false ) );
    mPalette->setColor( QPalette::Disabled, QPalette::AlternateBase,   generateColor( scheme, QPalette::Disabled, QPalette::AlternateBase, false ) );
    mPalette->setColor( QPalette::Disabled, QPalette::Link,            generateColor( scheme, QPalette::Disabled, QPalette::Link, true ) );
    mPalette->setColor( QPalette::Disabled, QPalette::LinkVisited,     generateColor( scheme, QPalette::Disabled, QPalette::LinkVisited, true ) );
    mPalette->setColor( QPalette::Disabled, QPalette::PlaceholderText, generateColor( scheme, QPalette::Disabled, QPalette::PlaceholderText, true ) );
    mPalette->setColor( QPalette::Disabled, QPalette::NoRole,          generateColor( scheme, QPalette::Disabled, QPalette::NoRole, false ) );
    mPalette->setColor( QPalette::Disabled, QPalette::BrightText,      generateColor( scheme, QPalette::Disabled, QPalette::BrightText, false ) );
}


void DFL::Config::ColorScheme::parseQt5CtColorScheme( QString file ) {
    QSettings   plttSett( file, QSettings::IniFormat );
    QStringList active   = plttSett.value( "ColorScheme/active_colors" ).toStringList();
    QStringList inactive = plttSett.value( "ColorScheme/inactive_colors" ).toStringList();
    QStringList disabled = plttSett.value( "ColorScheme/disabled_colors" ).toStringList();

    /** Load the colors for first 21 Roles.
     *  Qt < 6.6 had 21 roles (0-20)
     *  Qt >= 6.6 has 22 roles (0-21)
     *  The Newly added role is that of accent color.
     */
    for ( int i = 0; i < 21; i++ ) {
        mPalette->setColor( QPalette::Active,   QPalette::ColorRole( i ), QColor( active.at( i ) ) );
        mPalette->setColor( QPalette::Inactive, QPalette::ColorRole( i ), QColor( inactive.at( i ) ) );
        mPalette->setColor( QPalette::Disabled, QPalette::ColorRole( i ), QColor( disabled.at( i ) ) );
    }

    /** Support for QPalette::Accent */
    if ( QPalette::NColorRoles > 21 ) {
        mPalette->setColor( QPalette::Active,   QPalette::ColorRole( 21 ), mPalette->color( QPalette::Active, QPalette::Highlight ) );
        mPalette->setColor( QPalette::Inactive, QPalette::ColorRole( 21 ), mPalette->color( QPalette::Inactive, QPalette::Highlight ) );
        mPalette->setColor( QPalette::Disabled, QPalette::ColorRole( 21 ), mPalette->color( QPalette::Disabled, QPalette::Highlight ) );
    }
}
