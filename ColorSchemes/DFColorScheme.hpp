/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Config::ColorScheme class parses KDE and Qt5CT color schemes
 * and returns a QPalette for it.
 **/

#pragma once

#include <QColor>
#include <QString>
#include <QPalette>

namespace DFL {
    namespace Config {
        class ColorScheme;
    }
}

/**
 * Simple class to parse a given color schemes.
 * If the scheme ends with .conf it's our internal format.
 * If the scheme ends with .colors it's KDE format.
 */
class DFL::Config::ColorScheme {
    public:
        ColorScheme( QString scheme );

        /** Return the palette corresponding to the color scheme */
        QPalette palette();

        /** Additional search paths for color schemes */
        static void addColorSchemePaths( QStringList );

        /** List all the available color schemes */
        static QStringList colorSchemes();

    private:

        /**
         * First, look into ~/.config/DesQ/ColorSchemes/       <- User schemes
         * Next, look into {XDG_DATA_DIRS}/color-schemes/      <- KDE
         * Then, look into SharePath + desq/ColorSchemes/      <- Qt5ct Default
         */
        QString getSchemePath();

        /**
         * KDE Color Schemes are more complicated.
         * They contain multiple groups for use with KColorScheme
         * We will simply pick up what we need from them.
         */
        void parseKdeColorScheme( QString file );

        /**
         * We simply need to read out our palette colors
         */
        void parseQt5CtColorScheme( QString file );

        /** ColorScheme Name */
        QString mColorScheme;

        /** Pointer to the QPalette object */
        QPalette *mPalette = nullptr;

        /** Color scheme paths */
        static QStringList mSchemePaths;
};
