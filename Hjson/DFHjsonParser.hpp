/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Config::HjsonParser class parses a HJson file and returns the configuration
 * stored in it as a QVariantMap.
 **/

#pragma once

#include <QtCore>

namespace DFL {
    namespace Config {
        class Hjson;
    }
}

class DFL::Config::Hjson {
    public:
        enum Error {
            NoError,                    // There is no error
            TypeMismatchError,          // Error while converting from/to given type (internal error)
            IndexOutOfBounds,           // QList/QMap index was out of bounds
            SyntaxError,                // The syntax of the hjson file was incorrect.
            FileError,                  // The hjson file or folder was not found.
            UnknownError
        };

        /**
         * @file - File where the configuration is stored
         */
        Hjson( QString hjFile = "" );

        /**
         * Return the last encountered error.
         * A successful operation after an error, reset the error.
         */
        int error();

        /**
         * Get the configuration as QVariantMap.
         */
        QVariantMap config();

        /**
         * Load configuration options from @hjFile.
         * If configurations were loaded in during initialization, this function
         * will merge the configurations from @hjFile in the existing config.
         * If a key exists in mConfig, the corresponding value will be replaced by
         * the value from @hjFile.
         */
        int loadConfigFrom( QString hjFile );

        /**
         * Save the configuration loaded in the configuration to a @hjFile.
         */
        bool saveAs( QString hjFile );

        /**
         * Read the configuration stored in the file @filename,
         * and store them in QVariantMap object.
         * Note: Previously loaded configurations will not be modified, or used.
         * This function is provided for use with DFL::Settings.
         */
        static QVariantMap readConfigFromFile( QString filename );

        /**
         * Read the configuration stored in the file @filename,
         * and store them in QVariantMap object.
         * Note: Previously loaded configurations will not be modified, or used.
         * This function is provided for use with DFL::Settings.
         */
        static QVariantMap readConfigFromString( QString string );

        /**
         * Write the configuration stored QVariantMap object into a file @filename.
         * This function is provided for use with DFL::Settings.
         */
        static bool writeConfigToFile( QVariantMap settingMap, QString filename );

        /**
         * Write the configuration stored QVariantMap object into a file @filename.
         * This function is provided for use with DFL::Settings.
         */
        static QString writeConfigToString( QVariantMap settingMap );

    private:
        QVariantMap mConfig;

        /**
         * Stores the last reported error.
         */
        int mError = -1;
};
