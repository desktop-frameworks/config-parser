/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Config::HjsonParser class parses a ::Hjson file and returns the configuration
 * stored in it as a QVariantMap.
 **/

#include <cstring>

#include "HjsonUtils.hpp"

QVariant retrieveValue( ::Hjson::Value cfgVal ) {
    switch ( cfgVal.type() ) {
        case ::Hjson::Type::Bool: {
            return (bool)cfgVal;
        }

        case ::Hjson::Type::Double: {
            return (qreal)cfgVal.to_double();
        }

        case ::Hjson::Type::Int64: {
            return (qint64)cfgVal.to_int64();
        }

        case ::Hjson::Type::String: {
            return cfgVal.to_string().c_str();
        }

        default: {
            return QVariant();
        }
    }

    return QVariant();
}


QVariant retrieveVector( ::Hjson::Value cfgVal ) {
    QVariantList list;

    for ( size_t i = 0; i < cfgVal.size(); i++ ) {
        ::Hjson::Value val( cfgVal[ i ] );

        if ( val.is_container() ) {
            if ( val.type() == ::Hjson::Type::Vector ) {
                list << retrieveVector( val );
            }

            else {
                list << retrieveMap( val );
            }
        }

        else {
            list << retrieveValue( val );
        }
    }

    return list;
}


QVariant retrieveMap( ::Hjson::Value cfgVal ) {
    QVariantMap map;

    for ( size_t i = 0; i < cfgVal.size(); i++ ) {
        QString        key( cfgVal.key( i ).c_str() );
        ::Hjson::Value val( cfgVal[ key.toStdString() ] );

        if ( val.is_container() ) {
            if ( val.type() == ::Hjson::Type::Vector ) {
                map[ key ] = retrieveVector( val );
            }

            else {
                map[ key ] = retrieveMap( val );
            }
        }

        else {
            map[ key ] = retrieveValue( val );
        }
    }

    return map;
}


::Hjson::Value convertQVariant( QVariant cfgVal ) {
    switch ( (QMetaType::Type)cfgVal.type() ) {
        case QMetaType::Bool: {
            return ::Hjson::Value( cfgVal.toBool() );
        }

        case QMetaType::Short: {
            [[fallthrough]];
        }

        case QMetaType::Int: {
            return ::Hjson::Value( cfgVal.toInt() );
        }

        case QMetaType::UShort: {
            [[fallthrough]];
        }

        case QMetaType::UInt: {
            return ::Hjson::Value( cfgVal.toUInt() );
        }

        case QMetaType::Long: {
            [[fallthrough]];
        }

        case QMetaType::LongLong: {
            return ::Hjson::Value( cfgVal.toLongLong() );
        }

        case QMetaType::ULong: {
            [[fallthrough]];
        }

        case QMetaType::ULongLong: {
            return ::Hjson::Value( cfgVal.toULongLong() );
        }

        case QMetaType::Float: {
            return ::Hjson::Value( cfgVal.toFloat() );
        }

        case QMetaType::Double: {
            return ::Hjson::Value( cfgVal.toDouble() );
        }

        case QMetaType::QString: {
            return ::Hjson::Value( cfgVal.toString().toStdString() );
        }

        case QMetaType::QVariantList: {
            ::Hjson::Value vector  = ::Hjson::Value( ::Hjson::Type::Vector );
            QVariantList   varList = cfgVal.toList();
            for ( int i = 0; varList.size(); i++ ) {
                vector[ i ] = convertQVariant( varList[ i ] );
            }

            return vector;
        }

        case QMetaType::QVariantMap: {
            ::Hjson::Value map = ::Hjson::Value( ::Hjson::Type::Map );

            QVariantMap varMap = cfgVal.toMap();
            for ( QString key: varMap.keys() ) {
                map[ key.toStdString() ] = convertQVariant( varMap[ key ] );
            }

            return map;
        }

        default: {
            return ::Hjson::Value( ::Hjson::Type::Null );
        }
    }

    return ::Hjson::Value( ::Hjson::Type::Null );
}
