/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Config::HjsonParser class parses a ::Hjson file and returns the configuration
 * stored in it as a QVariantMap.
 **/

#include <dirent.h>
#include <unistd.h>
#include <sys/inotify.h>

#include <filesystem>
#include <cstring>
#include <set>

#include "hjson.h"
#include "HjsonUtils.hpp"
#include "DFHjsonParser.hpp"


DFL::Config::Hjson::Hjson( QString hjFile ) {
    mError = NoError;
    loadConfigFrom( hjFile );
}


int DFL::Config::Hjson::loadConfigFrom( QString hjFile ) {
    ::Hjson::Value hjCfg;

    try {
        hjCfg = ::Hjson::UnmarshalFromFile( hjFile.toStdString() );
    } catch ( ::Hjson::file_error& err ) {
        qDebug() << "Error reading configuration file.";
        qDebug() << err.what();

        mError = FileError;
        return FileError;
    } catch ( ::Hjson::syntax_error& err ) {
        qDebug() << "Error loading configuration file. Invalid syntax:";
        qDebug() << err.what();

        mError = SyntaxError;
        return SyntaxError;
    }

    if ( hjCfg.type() != ::Hjson::Type::Map ) {
        qCritical() << "Invalid configuration file";
        mError = SyntaxError;
    }

    mConfig.insert( retrieveMap( hjCfg ).toMap() );

    return NoError;
}


QVariantMap DFL::Config::Hjson::config() {
    return mConfig;
}


int DFL::Config::Hjson::error() {
    return mError;
}


QVariantMap DFL::Config::Hjson::readConfigFromFile( QString filename ) {
    DFL::Config::Hjson parser( filename );

    return parser.config();
}


QVariantMap DFL::Config::Hjson::readConfigFromString( QString data ) {
    ::Hjson::Value hjCfg;

    try {
        hjCfg = ::Hjson::Unmarshal( data.toStdString() );
    } catch ( ::Hjson::file_error& err ) {
        qDebug() << "Error reading the string";
        qDebug() << err.what();

        return QVariantMap();
    } catch ( ::Hjson::syntax_error& err ) {
        qDebug() << "Error loading configuration from string. Invalid syntax:";
        qDebug() << err.what();

        return QVariantMap();
    }

    if ( hjCfg.type() != ::Hjson::Type::Map ) {
        qCritical() << "Invalid configuration file";
    }

    return retrieveMap( hjCfg ).toMap();
}


bool DFL::Config::Hjson::writeConfigToFile( QVariantMap settingsMap, QString filename ) {
    ::Hjson::Value cfgMap;

    for ( QString key: settingsMap.keys() ) {
        cfgMap[ key.toStdString() ] = convertQVariant( settingsMap[ key ] );
    }

    ::Hjson::EncoderOptions encOpts;

    encOpts.indentBy      = "\t";
    encOpts.unknownAsNull = true;

    try {
        ::Hjson::MarshalToFile( cfgMap, filename.toStdString(), encOpts );
    } catch ( ::Hjson::file_error& err ) {
        qDebug() << "Error writing configuration file.";
        qDebug() << err.what();
        return false;
    }

    return true;
}


QString DFL::Config::Hjson::writeConfigToString( QVariantMap settingsMap ) {
    ::Hjson::Value cfgMap;

    for ( QString key: settingsMap.keys() ) {
        cfgMap[ key.toStdString() ] = convertQVariant( settingsMap[ key ] );
    }

    ::Hjson::EncoderOptions encOpts;

    encOpts.indentBy      = "\t";
    encOpts.unknownAsNull = true;

    std::string hjsonStr;

    try {
        hjsonStr = ::Hjson::Marshal( cfgMap, encOpts );
    } catch ( ::Hjson::file_error& err ) {
        qDebug() << "Error writing configuration file.";
        qDebug() << err.what();
        return "";
    }

    return QString( hjsonStr.c_str() );
}
