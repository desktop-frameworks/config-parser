/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * The DFL::Config::HjsonParser class parses a ::Hjson file and returns the configuration
 * stored in it as a QVariantMap.
 **/

#include <QVariant>

#include "hjson.h"

/**
 * Retrieve a simple value: Bool/Int64/Double/String
 */
QVariant retrieveValue( ::Hjson::Value cfgVal );

/**
 * Retrieve a vector
 */
QVariant retrieveVector( ::Hjson::Value cfgVal );

/**
 * Retrieve a map
 */
QVariant retrieveMap( ::Hjson::Value cfgVal );

/**
 * Convert a QVariant to Hjson::Value
 */
::Hjson::Value convertQVariant( QVariant cfgVal );
